#!/usr/bin/python3
import binascii
import sys

def rs232_checksum(the_bytes):
   return b'%02X' % (sum(the_bytes) & 0xFF)

if ( len(sys.argv) != 2 ):
   print ("Usage: ",sys.argv[0],"file.rmf")
   exit()

with open(sys.argv[1], "rb") as file:
   # Read first 9 Bytes as header 
   file_records=int.from_bytes(file.read(1), byteorder='little')
   file_Length=int.from_bytes(file.read(2), byteorder='little')
   file_version=int.from_bytes(file.read(1), byteorder='little')
   file_pad1=int.from_bytes(file.read(3), byteorder='little')
   file_0x09=int.from_bytes(file.read(1), byteorder='little')
   file_pad2=int.from_bytes(file.read(1), byteorder='little')
   data = file_records
   offset=0
   
   # Read each of the records
   while data:
      save_position=file.tell()

      offset=int.from_bytes(file.read(2), byteorder='little')
      offset_pad  = file.read(2)
      samplerate  = int.from_bytes(file.read(2), byteorder='little')
      sample_pad  = file.read(2)
      title  = file.read(16)
      data   = file.read(offset)
   
      if(title):
         title_1 = title[:title.find(b'\x00')]
         title_2 = title[title.find(b'\x00')+1:]
         title_2 = title_2[:title_2.find(b'\x00')]
         title=title_1.decode(encoding="ascii", errors="ignore")
         title_extra=title_2.decode(encoding="ascii", errors="ignore")
         assert( offset == len(data))
         print (">>",hex(offset), len(data) , ":", title )
         with open("dump_"+title+"_"+str(samplerate)+".rmf","wb") as rmf_write:
             rmf_write.write(data)
      else:
         file.seek(save_position)
         data   = file.read()
         assert( len(data) == 5 )
         if(data):
             print ("<<",binascii.hexlify(data),len(data))
             break
         else:
             print("Looks ok - EOF")
   
